<?php


function get_token(){
    $file = '/tmp/trumpit_cam_plugin_token.txt';
    if (!file_exists($file)) {
        //touch($file);
        return null;
    }
    //Open the File Stream
    $handle = fopen($file, "r+");
    if(flock($handle, LOCK_EX)) {
        $token = fread($handle, filesize($file));
        flock($handle, LOCK_UN); //Unlock File
        fclose($handle);
        return $token;
    }
    
    return null;
}

function set_token($token){
    $file = '/tmp/trumpit_cam_plugin_token.txt';

    //Open the File Stream
    $handle = fopen($file, "w");
    if(flock($handle, LOCK_EX)) {
        $token = fwrite($handle, $token);
        flock($handle, LOCK_UN); //Unlock File
        fclose($handle);
        return true;
    }
    
    return false;
}

    /*
    //Lock File, error if unable to lock
    if(flock($handle, LOCK_EX)) {
        $size = filesize($file);
        $count = $size === 0 ? 0 : fread($handle, $size); //Get Current Hit Count
        $count = $count + 1; //Increment Hit Count by 1
        echo $count;
        ftruncate($handle, 0); //Truncate the file to 0
        rewind($handle); //Set write pointer to beginning of file
        fwrite($handle, $count); //Write the new Hit Count
        flock($handle, LOCK_UN); //Unlock File
    } else {
        echo "Could not Lock File!";
    }

    //Close Stream
    fclose($handle);
*/
