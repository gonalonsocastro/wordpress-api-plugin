#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Comodo Partner API\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-01-28 15:50+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.0; wp-5.5.1\n"
"X-Domain: comodo-partner"

#: screens/cam_products.php:29
msgid ""
"An error has ocurred quering the product list database. Try later or contact "
"site manager"
msgstr ""

#: screens/cam_login.php:56
msgid "Can't access your account? Click here to reset your password"
msgstr ""

#: screens/cam_login.php:58
msgid "Click"
msgstr ""

#. Name of the plugin
msgid "Comodo Partner API"
msgstr ""

#. Description of the plugin
msgid "Comodo Partner API access"
msgstr ""

#: screens/cam_login.php:48
msgid "Email"
msgstr ""

#. Author of the plugin
msgid "Gonzalo Alonso"
msgstr ""

#. Author URI of the plugin
msgid "http://gonalonso.github.io"
msgstr ""

#. URI of the plugin
msgid "https://trumpit.ee"
msgstr ""

#: screens/cam_products.php:14
msgid "Invalid response"
msgstr ""

#: screens/index.php:10
msgid "Log Out"
msgstr ""

#: screens/index.php:14
msgid "Login"
msgstr ""

#: screens/cam_login.php:44
msgid "Login to your Comodo panel"
msgstr ""

#: screens/cam_login.php:51
msgid "Password"
msgstr ""

#: screens/index.php:5
msgid "Product list"
msgstr ""

#: screens/index.php:8
msgid "User serttings"
msgstr ""

#: screens/index.php:9
msgid "Your products"
msgstr ""
