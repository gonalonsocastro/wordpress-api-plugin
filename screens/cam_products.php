<?php
ob_start();
?>
<div id='cam_products_list'>
<h4>Comodo product list</h4>
<?php
    try {
        $productList = CAM::getProductList();
        
        if ($productList == null)
            throw new Exception('Invalid Product list');

        if( $productList['ErrorResponse'] )
            throw new Exception(__( 'Invalid response', 'comodo-partner'));
        
        echo "DEBUG:: ".var_dump( $productList );
        echo "<em id='cam_products_list_ul'>";

        
        foreach($productList  as $val){
            echo "<li>$val</li>\n";
        }
        
        echo "</em>";
    }
    catch (Exception $e) {
?>
    <div class='error cam_product_list_error'>
        <span><?php echo __('An error has ocurred quering the product list database. Try later or contact site manager','comodo-partner') ?></span>
        <br/><em><?php echo $e->getMessage() ?></em>
        <br/><em><?php echo var_dump($productList) ?></em>
    </div>
<?php
    }
?>
</div>

<?php

echo ob_get_clean();
return;

    
    
