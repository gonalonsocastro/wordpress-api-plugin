<?php

    $bad_email = false;

    if (isset($_POST['email'])) {
        $email = $_POST['email'];

        $return = CAM::resetUserPassword();
        
        if ($return == true) {
            ?>
            <div>
                <h3>Your request is sent. Check your email inbox for instructions</h3>
            </div>
            <?php
            echo ob_get_clean();
            return;
        }
        else {
            $bad_email = true;
        }
    }
    
    ?>
<h5>Enter your email address to reset your password</h5>
<form method='POST' action='?action=restore'>
    <input type='text' name='email' placeholder='Email' <?php
     if ($bad_email) echo "class='form_input_error'" ?>/>
    <button type='submit'>Enviar</button>
</form>
<?php

    echo ob_get_clean(); 