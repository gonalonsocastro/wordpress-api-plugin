<?php
    ob_start();

    // Check is logged-in => redirect
    $do_login = false;
    $form_correct = true;
    $bad_pass = false;
    $bad_email = false;

    
    if (isset($_POST['do_login'])){
        
        if (isset($_POST['email'])){
            $email = $_POST['email'];
        }
        else {
            $bad_email = true;
        }
        
        if (isset($_POST['password'])){
            $password = $_POST['password'];
        }
        else {
            $bad_pass = true;
        }

        // Try to authenticate on CAM Partner 

        $return = CAM::loadUserDetails();

        if ($return != null) {
            $_SESSION['user'] = 'user';
        ?>
        <script type="text/javascript">
            window.location = "?action=user";
        </script>
        <?php
        echo ob_get_clean();
        return;
        }

    }
    ?>
<h3><?php echo __('Login to your Comodo panel','comodo-partner')?></h3>
<form method='POST' action='?action=login'>
    <input type='hidden' name='do_login' value='1'>
    <input type='text' name='email'
    placeholder='<?php echo __('Email','comodo-partner')?>' <?php
     if ($bad_email) echo "class='form_input_error'" ?>/>
    <input type='password' name='password'
    placeholder='<?php echo __('Password','comodo-partner')?>'<?php
     if ($bad_pass) echo "class='form_input_error'" ?>/>
    <button type='submit'>Enviar</button>
</form>
<div>
    <p><?php echo __('Can\'t access your account? Click here to reset your password','comodo-partner')?>
    <a href="?action=restore">
    <?php echo __('Click','comodo-partner')?></a>
    </p>
</div>
<?php

    echo ob_get_clean(); 