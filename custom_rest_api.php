<?php

/*
add_action( 'rest_api_init', 'init_rest_api');
add_action( 'rest_api_init', 'registrar_suma_endpoint' );
*/
add_shortcode( 'trump-cam-partner', 'trump_cam_partner_func' );

function registrar_suma_endpoint() {
  register_rest_route( 
    'soyunignorante/v2',
    '/suma/',
    array(
      'method' => 'POST',
      'callback' => 'sumar_rest',
      'permission_callback' => function () {
        return true;
      }
    )
  );
}

function sumar_rest($request) {

  try {
      //echo (isset($token)) ? "Token valido" : "Token invalido";
      $token ='1';
      $resultado = "Token: $token\n";
      $params = $request->get_params();
      if (empty($sumando)) throw new Exception('Empty');
      //$resultado = intval( $sumando['n1'] ) + intval( $sumando['n2'] );
      
  }
  catch (Exception $e){
      return "Invalid POST values";
  }
  return $resultado;
}


function my_awesome_func( $data ) {
  $posts = get_posts( array(
    'author' => $data['id'],
  ) );
 
  if ( empty( $posts ) ) {
    return null;
  }
 
  return $posts[0]->post_title;
}



function init_rest_api() {
  register_rest_route( 'namespace/v1', '/endpoint/', array(//'/resource/(?P<id>\d+)', array(
    'methods' => 'GET',
    'callback' => 'my_awesome_func',
    'args' => array(
      'id' => array(
        'validate_callback' => 'is_numeric'
      ),
    ),
    'permission_callback' => function () {
      //return current_user_can( 'edit_others_posts' );
      return true;
    }
  ) );
}




function trump_cam_partner_func($atts){
    include('config.php');
    require_once 'comodo_api/comodo_partner_api.php';
    require_once 'lib/token_file.php';

/*    
    $a = shortcode_atts(
        array(
            'api_key' => '0',
            'secret_pwd' => '0',
            ),
        $atts );
*/

    if (!isset($secret_key))
      return "<em>Missing API_KEY</em>";
    


    $auth_token = get_token();

    if ($auth_token == null) {
        $auth_token = CAM::request_auth_token();
        //save_token($auth_token);
    }

    //$login = ['user'];

    if (!isset( $_GET['action'])) 
        require 'screens/index.php';

    elseif ($_GET['action'] == 'login')
        require 'screens/cam_login.php';
    
    elseif ($_GET['action'] == 'products')
        require 'screens/cam_products.php';
        
    elseif ($_GET['action'] == 'user')
        require 'screens/cam_customer.php';

    elseif ($_GET['action'] == 'restore')
        require 'screens/cam_reset_password.php';

    elseif ($_GET['action'] == 'form')
        require 'screens/test_form.php';
        
    else require 'screens/not_found.php';
}


