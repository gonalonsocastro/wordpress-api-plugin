<?php
/*
 * Plugin Name: Comodo Partner API
 * Plugin URI: https://trumpit.ee
 * Description: Comodo Partner API access
 * Author: Gonzalo Alonso
 * Version: 0.0.1
 * Author URI: http://gonalonso.github.io
 * Text Domain: comodo-partner
 * Domain Path: /languages
 */


function register_session()
{
  if( !session_id() )
    session_start();
}

add_action('init', 'register_session');

require_once "comodo_api/comodo_partner_api.php";
require 'custom_rest_api.php';


function trump_cam_partner_plugin_setup_post_type() {
    register_post_type( 'trumpit_cam_partner', ['public' => true ] );
}
add_action( 'init', 'trump_cam_partner_plugin_setup_post_type' );


/**
 * Activate the plugin.
 */
function trump_cam_partner_plugin_activate() { 
    trump_cam_partner_plugin_setup_post_type(); 
    flush_rewrite_rules(); 
}
register_activation_hook( __FILE__, 'trump_cam_partner_plugin_activate' );

/**
 * Deactivation hook.
 */
function trump_cam_partner_plugin_deactivate() {
    unregister_post_type( 'trumpit_cam_partner' );
    flush_rewrite_rules();
}
register_deactivation_hook( __FILE__, 'trump_cam_partner_plugin_deactivate' );


function trump_cam_partner_plugin_uninstall() {
    // Delete database content belonging to this plugin
}
register_uninstall_hook(__FILE__, 'trump_cam_partner_plugin_uninstall');
